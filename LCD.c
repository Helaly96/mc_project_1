#include "LCD.h"
#include <stdlib.h>
#include <stdio.h>
void LCD_Command(char command)
{
	// assume that port B is the port connected to the data of LCD
	// and assume that port A pins Rs=5,Rw=6,EN=7
	
	//since we are writing,all is zero.
	 DIO_WritePort( 1 , 0xFF ,  STD_LOW);
	 GPIO_PORTB_DATA_R = (command);
	 //Enable
	 GPIO_PORTA_DATA_R |=	0x80;
	 sys_tic_delay_ms(100);
	 GPIO_PORTA_DATA_R &=	~0x80;	 
}

void LCD_Data(char command)
{
	// assume that port B is the port connected to the data of LCD
	// and assume that port A pins Rs=5,Rw=6,EN=7
	
	
	 GPIO_PORTA_DATA_R = 0x20;
	 GPIO_PORTB_DATA_R = command;
	 //Enable
	 GPIO_PORTA_DATA_R |=	0x80;
	 sys_tic_delay_ms(100);
	 GPIO_PORTA_DATA_R &=	~0x80;	 
}

void LCD_init()
{
			LCD_Command(1);
			LCD_Command(0x30);
			LCD_Command(0x80);
			LCD_Command(0x0F);
}

void send_counter_to_lcd(int counter)
{
		char num1;
		char num2;
		char num3;
		
		int left = (counter / 100);
		int middle= (counter - left*100)/10;
		int right = (counter-(left*100)-(middle*10));
		
		num1 = left + 48;
		num2 = middle + 48;
		num3=  right + 48;
		LCD_Command(1);
		LCD_Data(num1);
		LCD_Data(num2);
		LCD_Data(num3);
	
	
}