#include "LCD.h"
#define GPIO_PF2_M					0x04U		// Pin 2 is connected to blue
//testing code 
#define GPIO_PF1_M					0x02U		// Pin 1 is connected to red
#define GPIO_PF4_M					0x10		// Pin 4 is connected to switch
#define GPIO_PF3_M					0x08U		// Pin 3 is connected to green
//


void SystemInit(void){
	
	// prepare the sys-tic timer but only need to be initialized
	sys_tic_init(319999);
	// initialize port A & B , to make it connected to LCD
	//Port_Init(1);
	//Port_Init(2);

	Port_Init(32);
	Port_Init(2);
	Port_Init(1);
	
	
	//Intialize Port A & B as output port to connect it to LCD
	Port_SetPinDirection(2,0xFF,PORT_PIN_OUT);
	Port_SetPinDirection(1,0xFF,PORT_PIN_OUT);
	Port_SetPinDirection(32,0x02,PORT_PIN_IN);
	Port_SetPinDirection(32,0x04,PORT_PIN_IN);
	Port_SetPinDirection(32,0x08,PORT_PIN_IN);
	Port_SetPinPullUp(32,0x02,1);
	Port_SetPinPullUp(32,0x04,1);
	Port_SetPinPullUp(32,0x08,1);
	//lcd init
	LCD_init();

	
}


int main(void){
	int counter=0;
	int flag=1;
	int flag2=1;
	uint8_t please_work;
	uint8_t please_work2;
	uint8_t please_work3;
	LCD_Command(1);
	for(;;)
	  {
			 please_work=GPIO_PORTF_DATA_R&0x02;	
			 please_work2=GPIO_PORTF_DATA_R&0x04;
			 please_work3=GPIO_PORTF_DATA_R&0x08;
				
			 if ( please_work != 0x02 )
			 {
				counter++;
				sys_tic_delay_ms(100);
				LCD_Command(1); 
				send_counter_to_lcd( counter);
			 }
			 
			 else if ( please_work2 != 0x04 && flag==1 )
			 {
				counter--;
				send_counter_to_lcd( counter); 
				flag=0;
			 }
			 
			  else if ( please_work3 != 0x08 && flag2==1 )
			 {
				 LCD_Command(1);
				counter=0;
				
				flag2=0;
			 }
			 
			 if (please_work2 == 0x04 && flag==0) {flag=1;}
			 if (please_work3 == 0x08 && flag2==0) {send_counter_to_lcd( counter);flag2=1;}



		}
	
}