#include "PortHandler.h"

void intialize_port_a()
{
	//Enables the clock for that port
	SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R0;
	//unlocks the port
	GPIO_PORTA_LOCK_R = GPIO_LOCK_KEY; 
	// allow changes to the ports
	GPIO_PORTA_CR_R |= GPIO_PORT_ALLOW_ALL;
	//Disable Analog fn
	GPIO_PORTA_AMSEL_R &= ~GPIO_PORT_ALLOW_ALL;
	// No alternate function
	GPIO_PORTA_AFSEL_R &= ~GPIO_PORT_ALLOW_ALL;
	//Yes, they are ALL GPIO
	 GPIO_PORTA_PCTL_R &= ~GPIO_PORT_ALLOW_ALL;
	//make them digital bardo
	GPIO_PORTA_DEN_R |= GPIO_PORT_ALLOW_ALL; 
	// intialize all pins to have zero
	GPIO_PORTA_DATA_R &= ~GPIO_PORT_ALLOW_ALL;	
	
	
}
void intialize_port_b()
{
	//Enables the clock for that port
	SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R1;
	//unlocks the port
	GPIO_PORTB_LOCK_R = GPIO_LOCK_KEY; 
	// allow changes to the ports
	GPIO_PORTB_CR_R |= GPIO_PORT_ALLOW_ALL;
	//Disable Analog fn
	GPIO_PORTB_AMSEL_R &= ~GPIO_PORT_ALLOW_ALL;
	// No alternate function
	GPIO_PORTB_AFSEL_R &= ~GPIO_PORT_ALLOW_ALL;
	//Yes, they are ALL GPIO
	 GPIO_PORTB_PCTL_R &= ~GPIO_PORT_ALLOW_ALL;
	//make them digital bardo
	GPIO_PORTB_DEN_R |= GPIO_PORT_ALLOW_ALL;
	// intialize all pins to have zero
	GPIO_PORTB_DATA_R &= ~GPIO_PORT_ALLOW_ALL;
	
}

void intialize_port_c()
{
	//Enables the clock for that port
	SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R2;
	//unlocks the port
	GPIO_PORTC_LOCK_R = GPIO_LOCK_KEY; 
	// allow changes to the ports
	GPIO_PORTC_CR_R |= GPIO_PORT_ALLOW_ALL;
	//Disable Analog fn
	GPIO_PORTC_AMSEL_R &= ~GPIO_PORT_ALLOW_ALL;
	// No alternate function
	GPIO_PORTC_AFSEL_R &= ~GPIO_PORT_ALLOW_ALL;
	//Yes, they are ALL GPIO
	 GPIO_PORTC_PCTL_R &= ~GPIO_PORT_ALLOW_ALL;
	//make them digital bardo
	GPIO_PORTC_DEN_R |= GPIO_PORT_ALLOW_ALL;
	// intialize all pins to have zero
	GPIO_PORTC_DATA_R &= ~GPIO_PORT_ALLOW_ALL;
	
}

void intialize_port_d()
{
 //Enables the clock for that port	
 SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R3;
	//unlocks the port
	GPIO_PORTD_LOCK_R = GPIO_LOCK_KEY; 
	// allow changes to the ports
	GPIO_PORTD_CR_R |= GPIO_PORT_ALLOW_ALL;
	//Disable Analog fn
	GPIO_PORTD_AMSEL_R &= ~GPIO_PORT_ALLOW_ALL;
	// No alternate function
	GPIO_PORTD_AFSEL_R &= ~GPIO_PORT_ALLOW_ALL;
	//Yes, they are ALL GPIO
	 GPIO_PORTD_PCTL_R &= ~GPIO_PORT_ALLOW_ALL;
	//make them digital bardo
	GPIO_PORTD_DEN_R |= GPIO_PORT_ALLOW_ALL;
	// intialize all pins to have zero
	GPIO_PORTD_DATA_R &= ~GPIO_PORT_ALLOW_ALL;
	
}

void intialize_port_e()
{
 //Enables the clock for that port	
  SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R4;
	//unlocks the port
	GPIO_PORTE_LOCK_R = GPIO_LOCK_KEY; 
	// allow changes to the ports
	GPIO_PORTE_CR_R |= GPIO_PORT_ALLOW_ALL;
	//Disable Analog fn
	GPIO_PORTE_AMSEL_R &= ~GPIO_PORT_ALLOW_ALL;
	// No alternate function
	GPIO_PORTE_AFSEL_R &= ~GPIO_PORT_ALLOW_ALL;
	//Yes, they are ALL GPIO
	 GPIO_PORTE_PCTL_R &= ~GPIO_PORT_ALLOW_ALL;
	//make them digital bardo
	GPIO_PORTE_DEN_R |= GPIO_PORT_ALLOW_ALL;
	// intialize all pins to have zero
	GPIO_PORTE_DATA_R &= ~GPIO_PORT_ALLOW_ALL;
	
	
}

void intialize_port_f()
{
	//Enables the clock for that port
	SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R5;
	//unlocks the port
	GPIO_PORTF_LOCK_R = GPIO_LOCK_KEY; 
	// allow changes to the ports
	GPIO_PORTF_CR_R |= GPIO_PORT_ALLOW_ALL;
	//Disable Analog fn
	GPIO_PORTF_AMSEL_R &= ~GPIO_PORT_ALLOW_ALL;
	// No alternate function
	GPIO_PORTF_AFSEL_R &= ~GPIO_PORT_ALLOW_ALL;
	//Yes, they are ALL GPIO
	 GPIO_PORTF_PCTL_R &= ~GPIO_PORT_ALLOW_ALL;
	//make them digital bardo
	GPIO_PORTF_DEN_R |= GPIO_PORT_ALLOW_ALL;
	// intialize all pins to have zero
	GPIO_PORTF_DATA_R &= ~GPIO_PORT_ALLOW_ALL;
	
	
}

void Port_Init( uint8_t port_index )
{
	switch (port_index)
{
    case 1:  intialize_port_a();
    case 2:  intialize_port_b();
    case 4:  intialize_port_c();
		case 8:  intialize_port_d();
		case 16: intialize_port_e();	
    case 32: intialize_port_f();  
}

}



void Port_SetPinDirection(uint8_t port_index , uint8_t pins_mask , Port_PinDirectionType pins_direction)
{
	
	
	if(pins_direction == PORT_PIN_IN)
	{
		// that means those pins in the mask will be input pins
		switch (port_index)
		{
    case 1:  GPIO_PORTA_DIR_R &= ~pins_mask; 
    case 2:  GPIO_PORTB_DIR_R &= ~pins_mask;  
    case 4:  GPIO_PORTC_DIR_R &= ~pins_mask;  
		case 8:  GPIO_PORTD_DIR_R &= ~pins_mask; 
		case 16: GPIO_PORTE_DIR_R &= ~pins_mask;  	
    case 32: GPIO_PORTF_DIR_R &= ~pins_mask;   
		}
		
		
	}
	else if(pins_direction == PORT_PIN_OUT)
	{
		// that means those pins in the mask will be output pins
		switch (port_index)
		{
		case 1:  GPIO_PORTA_DIR_R |= pins_mask; 
    case 2:  GPIO_PORTB_DIR_R |= pins_mask;  
    case 4:  GPIO_PORTC_DIR_R |= pins_mask; 
		case 8:  GPIO_PORTD_DIR_R |= pins_mask; 
		case 16: GPIO_PORTE_DIR_R |= pins_mask;  	
    case 32: GPIO_PORTF_DIR_R |= pins_mask; 
	  }
	
}

}

void Port_SetPinPullUp( uint8_t port_index , uint8_t pins_mask , uint8_t enable)
{
	if (enable ==1)
	{
		switch (port_index)
		{
		case 1:  GPIO_PORTA_PUR_R |= pins_mask; 
    case 2:  GPIO_PORTB_PUR_R |= pins_mask;  
		case 4:  GPIO_PORTC_PUR_R |= pins_mask;
		case 8:  GPIO_PORTD_PUR_R |= pins_mask;
		case 16: GPIO_PORTE_PUR_R |= pins_mask;  	
    case 32: GPIO_PORTF_PUR_R |= pins_mask; 
	  }
	}
	else if (enable == 0)
	{
		switch (port_index)
		{
		case 1:  GPIO_PORTA_PUR_R &= ~pins_mask; 
    case 2:  GPIO_PORTB_PUR_R &= ~pins_mask;  
		case 4:  GPIO_PORTC_PUR_R &= ~pins_mask;	
		case 8:  GPIO_PORTD_PUR_R &= ~pins_mask;
		case 16: GPIO_PORTE_PUR_R &= ~pins_mask;  	
    case 32: GPIO_PORTF_PUR_R &= ~pins_mask; 
	  }
	}
	
}

void Port_SetPinPullDown( uint8_t port_index , uint8_t pins_mask , uint8_t enable)
{
	if (enable ==1)
	{
		switch (port_index)
		{
		case 1:  GPIO_PORTA_PDR_R   |= pins_mask; 
    case 2:  GPIO_PORTB_PDR_R   |= pins_mask;
		case 4:  GPIO_PORTC_PDR_R   |= pins_mask;			
		case 8:  GPIO_PORTD_PDR_R   |= pins_mask;
		case 16: GPIO_PORTE_PDR_R   |= pins_mask;  	
    case 32: GPIO_PORTF_PDR_R   |= pins_mask; 
	  }
	}
	else if (enable == 0)
	{
		switch (port_index)
		{
		case 1:  GPIO_PORTA_PDR_R   &= ~pins_mask; 
    case 2:  GPIO_PORTB_PDR_R   &= ~pins_mask;  
		case 4:  GPIO_PORTC_PDR_R   |= pins_mask;
		case 8:  GPIO_PORTD_PDR_R   &= ~pins_mask;
		case 16: GPIO_PORTE_PDR_R   &= ~pins_mask;  	
    case 32: GPIO_PORTF_PDR_R   &= ~pins_mask; 
	  }
	}
	
}

uint8_t DIO_ReadPort(uint8_t port_index ,uint8_t pins_mask)
{
	switch (port_index)
		{
		case 1:  return GPIO_PORTA_DATA_R & pins_mask;  
    case 2:  return GPIO_PORTB_DATA_R & pins_mask; 
		case 4:  return GPIO_PORTC_DATA_R & pins_mask; 	
		case 8:  return GPIO_PORTD_DATA_R & pins_mask;
		case 16: return GPIO_PORTE_DATA_R & pins_mask; 
    case 32: return GPIO_PORTF_DATA_R & pins_mask;
			default: return -1;
	  }
}

void DIO_WritePort(uint8_t port_index ,uint8_t pins_mask , Dio_LevelType pins_level)
{
	if(pins_level == STD_LOW)
	{
		switch (port_index)
		{
		case 1:   GPIO_PORTA_DATA_R &= ~pins_mask;  
    case 2:   GPIO_PORTB_DATA_R &= ~pins_mask;  
		case 4:   GPIO_PORTC_DATA_R &= ~pins_mask;  	
		case 8:   GPIO_PORTD_DATA_R &= ~pins_mask;  
		case 16:  GPIO_PORTE_DATA_R &= ~pins_mask;  
    case 32:  GPIO_PORTF_DATA_R &= ~pins_mask;  
	  }
	}
	else
	{
		switch (port_index)
		{
		case 1:   GPIO_PORTA_DATA_R |= pins_mask;  
    case 2:   GPIO_PORTB_DATA_R |= pins_mask; 
		case 4:   GPIO_PORTC_DATA_R |= pins_mask;	
		case 8:   GPIO_PORTD_DATA_R |= pins_mask;
		case 16:  GPIO_PORTE_DATA_R |= pins_mask; 
    case 32:  GPIO_PORTF_DATA_R |= pins_mask;
		
	  }
	}
	
}

void DIO_FlipPort(uint8_t port_index ,uint8_t pins_mask)
{
	switch (port_index)
		{
		case 1:   GPIO_PORTA_DATA_R ^= pins_mask;  
    case 2:   GPIO_PORTB_DATA_R ^= pins_mask; 
		case 4:   GPIO_PORTC_DATA_R ^= pins_mask;	
		case 8:   GPIO_PORTD_DATA_R ^= pins_mask;
		case 16:  GPIO_PORTE_DATA_R ^= pins_mask; 
    case 32:  GPIO_PORTF_DATA_R ^= pins_mask;
			
	  }
}
