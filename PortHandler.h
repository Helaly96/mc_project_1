#include "tm4c123gh6pm.h"
#define GPIO_PORT_ALLOW_ALL				0xFFU 	// Allows changes to all ports
#include <stdint.h> // to use the types like uint8_t and so on
//TypeDefs
typedef enum
{
    PORT_PIN_IN,
    PORT_PIN_OUT
}Port_PinDirectionType;

typedef enum
{
    STD_LOW,
    STD_HIGH
}Dio_LevelType;



//Funcutions
void intialize_port_a();
void intialize_port_b();
void intialize_port_c();
void intialize_port_d();
void intialize_port_d();
void intialize_port_e();
void intialize_port_f();
void Port_Init( uint8_t port_index );
void Port_SetPinDirection(uint8_t port_index , uint8_t pins_mask , Port_PinDirectionType pins_direction);
void Port_SetPinPullUp(uint8_t port_index , uint8_t pins_mask , uint8_t enable);
void Port_SetPinPullDown(uint8_t port_index , uint8_t pins_mask , uint8_t enable);
uint8_t DIO_ReadPort(uint8_t port_index ,uint8_t pins_mask);
void DIO_WritePort(uint8_t port_index ,uint8_t pins_mask , Dio_LevelType pins_level);


