#include "PortHandler.h"

void sys_tic_init( int counts_required );
void sys_tic_enable();
void sys_tic_disable();
void sys_tic_delay_ns(uint32_t n);
void sys_tic_delay_ms(uint32_t n);
